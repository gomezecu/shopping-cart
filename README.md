# Shopping Cart

Shopping Cart for Software Design I (MDAS La Salle)

### Contributors:

ezequiel.gomez@students.salle.url.edu
perepadial@gmail.com
pau.gv@students.salle.url.edu
keven.sa17@gmail.com
jose.ferrer@students.salle.url.edu

## Iniciar applicación 

1. Instalar dependencias
```
npm install
```
2. Importar datos 
```
npm run db:init
```
3. Lanzar tests
```
npm run test
```
4. Iniciar servicio 
```
npm start
```
## Datos iniciales

Tras la importación de datos tendrás acceso ha los siguientes registros: 

**productos**

| id       | name           | giftedWith | stock | price | currency |
|----------|----------------|------------|-------|-------|----------|
| P-000010 | productTest110 | P-000008   | 1     | 1     | EUR      |
| P-000001 | productTest123 |            | 10    | 5     | EUR      |
| P-000009 | productTest199 |            | 1     | 5.78  | EUR      |
| P-000004 | productTest134 |            | 0     | 8.99  | EUR      |
| P-000002 | productTest122 |            | 3     | 9.13  | EUR      |
| P-000003 | productTest133 |            | 0     | 9.99  | EUR      |
| P-000005 | productTest135 |            | 5     | 89    | EUR      |
| P-000006 | productTest166 |            | 3     | 450   | EUR      |
| P-000007 | productTest177 |            | 7     | 500   | EUR      |
| P-000008 | productTest188 |            | 9     | 578   | EUR      |

*El producto P-000010 va de regalo cuando el usuario compra el producto P-000008*

**shopping carts**

|             id            | total_amount | is_complete | currency | coupon_id |
|:-------------------------:|:------------:|:-----------:|:--------:|:---------:|
| asdlfjlksdajfqo2ie-askdfj | 20           |           1 | EUR      | FIXED_10  |

**cart entries**

| id |          cart_id          | product_id | quantity |
|:--:|:-------------------------:|:----------:|:--------:|
|    | asdlfjlksdajfqo2ie-askdfj | P-000001   | 2        |

**currencies**

| code |  name  | is_base |
|:----:|:------:|:-------:|
| EUR  | euro   |       1 |
| USD  | dollar |       0 |

**coupons**

|    code    | type | discount | min_total | is_active |
|:----------:|:----:|:--------:|:---------:|:---------:|
| FIXED_10   | F    | 10       | 10        |         0 |
| PERCENT_50 | P    | 50       | 15        |         1 |

## Configuración postman

1. Importar proyecto [postman collection](./Cart.postman_collection.json)

2. Importar variables de entorno postman [postman environment](./Local.postman_environment.json)

3. Seleccionar Local como variable de entorno:

![postman environment](images/postman-environment.png)
