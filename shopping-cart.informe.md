# Informe Shopping-Cart

## Decisiones de diseño y arquitectura

### Diagrama de clases

![database diagram](images/class-diagram.png)

### Diagrama de Base de datos

![database diagram](images/database-diagram.png)

### Bus de eventos

Para la realizacion de el registro de eventos de dominio, hemos utilizado un bus de eventos con una interface Subscriber. A partir de esta implementación
conseguimos que en cada caso de uso que se relacione con la entidad de Cart se guarde este evento. Nuestra entidad principal Cart
implementa una clase abstracta AggregateRoot la cual se encarga de registrar los eventos que nosotros le indiquemos por cada caso de uso.  
Adicionalmente cada controlador que necesite un registro de evento implementa la interfaz DomainEventSubscriber con la cual podrá guardar el evento en
base de datos.

## Herramientas y tecnologías

Para la realización de este proyecto se eligió como entorno para la capa de servidor NodeJs, pues la mayoría de los integrantes teníamos
como lenguaje principal Javascript y conocíamos este entorno.

Para facilitar el trabajo a la hora de crear los controladores en esta aplicación de NodeJs, decidimos hacer uso de Express.js como
framework: fácil de implementar y completo.

Se escogió como lenguaje principal Typescript, al ser un lenguaje altamente tipado y construido por encima de Javascript. Creíamos que
era obligatorio el uso de un lenguaje altamente tipado como Typescript para la construcción de esta aplicación, pues la arquitectura
hexagonal parecía exigirnos ciertas características o funcionalidades, como es el caso paradigmático de las "interfaces".

Para el trabajo relacionado con la persistencia de datos, hemos optado por una biblioteca llamada SQLite, al ser compatible con ACID,
ligera y de fácil implementación en NodeJs.

En cuanto a la biblioteca para test, hacemos uso de Jest, una herramienta que ofrece, al igual que la mayoría de las tecnologías antes
mencionadas, facilidad de uso y de instalación y buena documentación.

Para lo relacionado con el bus de eventos, hicimos uso de pubsub-js.

Hacemos empleo de Husky, Eslint y Prettier para facilitar la homogeneización del trabajo individual, tanto de las descripciones de los
commits como de la guía de estilos del código.

## Conclusiones sobre el aprendizaje

En línea generales, este proyecto ha cumplido íntegramente con su principal propósito: el aprendizaje introductorio al mundo de la
arquitectura hexagonal. A medida que implementábamos funcionalidades, hemos sigo testigos que una de las principales bonanzas de este
tipo de arquitectura es que, de alguna manera, te "obliga" a ir implementando todo lo relacionado con buenas prácticas (clean code) y
ciertos patrones de diseño (tales como factories, etc.) a medida que se desarrolla. Si entendemos arquitectura como una "guía" o "base"
por la cual resolver problemas relacionados con la programación, nos ha parecido completamente útil.

Al inicio, su implementación es compleja, pues tuvimos que aprender varios conceptos desconocidos hasta el momento (tales como Value
Objects, Entidades, Comandos, etc.), pero una vez que desarrollamos un par de casos de uso, el resto resulto mucho más sencillo.

Algo que nos ha gustado mucho es que hemos podido aplicar algo que al inicio de las clases nos parecía muy extraño: la utilización de un
archivo para una única funcionalidad; clases con un nombre claro y explícito, en cuanto a su funcionalidad se refiere y con un único método
execute nos ha parecido muy positivo.

Aprendimos no solamente qué es un Value Object y una Entidad, sino también a diferenciarlos en situaciones que, en un principio, resultaban
confusas o ambiguas.

Algo que nos constó un poco fue saber cómo manejar los errores, decidir qué responsabilidad tenía cada capa (al inicio era confusa la
capa de dominio y la de aplicación) o donde se debía realizar la instanciación de las clases.

En líneas generales, fue una experiencia muy positiva y constructiva, en la que la arquitectura hexagonal nos ha ido muy bien para ser
testigos de muchas de las buenas prácticas que tenemos que tener en cuenta como developers, así como patrones de diseño que nos pueden
ayudar a tener un código más legible y mantenible.
