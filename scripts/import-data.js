const sqlite3 = require('sqlite3')
const sqlite = require('sqlite')
const fs = require('fs').promises

require('dotenv').config()

const { DB_NAME } = process.env

async function runScript() {
  try {
    const db = await sqlite.open({
      filename: DB_NAME,
      mode: sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE,
      driver: sqlite3.Database
    })
    const fileData = await fs.readFile('./dataFiles/products.json')
    const jsonData = JSON.parse(fileData)
    const products = jsonData.products
    await insertItemsInBulk('INSERT INTO Product(id, name, stock, currency, price, giftedWith) VALUES', products, db)

    await db.run('INSERT INTO currency (code, name, is_base) VALUES (?, ?, ?)', ['EUR', 'euro', true])
    await db.run('INSERT INTO currency (code, name, is_base) VALUES (?, ?, ?)', ['USD', 'dollar', false])

    await db.run('INSERT INTO coupon (code, type , discount, min_total, is_active) VALUES(?, ?, ?, ?, ?)', ['FIXED_10', 'F', 10, 10, false])
    await db.run('INSERT INTO coupon (code, type , discount, min_total, is_active) VALUES(?, ?, ?, ?, ?)', ['PERCENT_50', 'P', 50, 15, true])

    await db.run('INSERT INTO shopping_cart(id, total_amount , is_complete , currency , coupon_id )VALUES(?, ?, ?, ?, ? )', ['asdlfjlksdajfqo2ie-askdfj', 20, true, 'EUR', 'FIXED_10'])

    await db.run('INSERT INTO cart_entry (cart_id, product_id, quantity) VALUES (?, ?, ?)', ['asdlfjlksdajfqo2ie-askdfj', 'P-000001', 2])
    db.close()
  } catch (e) {
    console.log(e)
  }
}

runScript()

async function insertItemsInBulk(query, itemList, db) {
  const fieldsToInsert = getObjectFieldsToBeInserted(itemList)
  const flatteredFieldsToInsert = fieldsToInsert.flat()
  const numberOfFieldsPerObject = fieldsToInsert[0].length
  const placeholders = getQueryPlaceHolders(fieldsToInsert, numberOfFieldsPerObject)
  const sql = `${query}${placeholders}`
  await db.run(sql, flatteredFieldsToInsert)
}

function getObjectFieldsToBeInserted(objectList) {
  const result = []
  for (const object of objectList) {
    const objectParameterList = []
    for (const objectProperty in object) {
      objectParameterList.push(object[objectProperty])
    }
    result.push(objectParameterList)
  }
  return result
}

function getQueryPlaceHolders(objectList, numberOfFields) {
  const fieldsQuery = []
  for (let i = 0; i < numberOfFields; i++) {
    fieldsQuery.push('?')
  }
  const fieldsQueryString = fieldsQuery.join(',')
  return objectList.map(() => `(${fieldsQueryString})`).join(',')
}
