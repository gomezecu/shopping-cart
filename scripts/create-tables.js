const sqlite3 = require('sqlite3')
const sqlite = require('sqlite')

require('dotenv').config()

const { DB_NAME } = process.env

async function runScript() {
  try {
    const db = await sqlite.open({
      filename: DB_NAME,
      mode: sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE,
      driver: sqlite3.Database
    })
    await db.run(
      'CREATE TABLE IF NOT EXISTS coupon(code text PRIMARY KEY UNIQUE, type text, discount number, min_total number, is_active boolean)'
    )
    await db.run(
      'CREATE TABLE IF NOT EXISTS shopping_cart(id text PRIMARY KEY UNIQUE, total_amount number, is_complete boolean, currency text, coupon_id text, FOREIGN KEY (coupon_id) REFERENCES coupon(code) ON UPDATE CASCADE ON DELETE NO ACTION)'
    )
    await db.run(
      'CREATE TABLE IF NOT EXISTS product(id text PRIMARY KEY UNIQUE, name text, giftedWith text, stock number, price number, currency text, FOREIGN KEY (giftedWith) REFERENCES product(id) ON UPDATE CASCADE ON DELETE NO ACTION)'
    )
    await db.run(
      'CREATE TABLE IF NOT EXISTS cart_entry(' +
      'id text PRIMARY KEY UNIQUE,' +
      'cart_id text,' +
      'product_id text, ' +
      'quantity number,' +
      'FOREIGN KEY (product_id) REFERENCES product(id) ON UPDATE CASCADE ON DELETE NO ACTION,' +
      'FOREIGN KEY (cart_id) REFERENCES shopping_cart(id) ON UPDATE CASCADE ON DELETE CASCADE' +
      ')'
    )
    await db.run('CREATE TABLE IF NOT EXISTS cart_logs(action text, date Date,cart_id text,details text)')
    await db.run('CREATE TABLE IF NOT EXISTS currency(code text, name text, is_base boolean)')

    db.close()
  } catch (e) {
    console.log(e)
  }
}

runScript()
