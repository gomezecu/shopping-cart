import CartLogEvent from 'domain/CartLogEvent'
import { CartLogRepository } from 'domain/repositories'
import { DbConnection } from 'infrastructure/db'

export default class SQLiteCartLogRepository implements CartLogRepository {
  async saveEvent(cartLogEvent: CartLogEvent) {
    const db = await DbConnection.open()

    const result = await db.run('INSERT INTO cart_logs (action, date, cart_id, details) VALUES (?, ?, ?, ?)', [
      cartLogEvent.action,
      cartLogEvent.date,
      cartLogEvent.cartId,
      cartLogEvent.details
    ])

    db.close()

    return result
  }
}
