/* eslint-disable camelcase */
/* eslint-disable indent */
import { UniqueEntityID } from 'domain/base'
import Coupon from 'domain/Coupon'
import CouponFixed from 'domain/CouponFixed'
import CouponPercent from 'domain/CouponPercent'
import { CouponAmount, CouponCode, CouponMinTotal } from 'domain/value-objects'

import { Mapper } from 'infrastructure/core'

export default class CouponMap implements Mapper<Coupon> {
  toDomain({ code, id, discount, min_total, type, is_active }: any): Coupon {
    const data = {
      id: new UniqueEntityID(id),
      discount: new CouponAmount(discount),
      minTotal: new CouponMinTotal(min_total),
      code: new CouponCode(code),
      isActive: is_active
    }
    return type === 'P' ? new CouponPercent(data) : new CouponFixed(data)
  }

  toPersistence(t: Coupon) {
    throw new Error('Method not implemented.')
  }
}
