/* eslint-disable camelcase */
import { UniqueEntityID } from 'domain/base'
import Currency from 'domain/Currency'
import { CurrencyCode, CurrencyName } from 'domain/value-objects'
import { Mapper } from 'infrastructure/core'

export default class CurrencyMap implements Mapper<Currency> {
  toDomain({ code, id, is_base, name }: any): Currency {
    return new Currency({
      code: new CurrencyCode(code),
      id: new UniqueEntityID(id),
      isBase: is_base,
      name: new CurrencyName(name)
    })
  }

  toPersistence(t: Currency) {
    throw new Error('Method not implemented.')
  }
}
