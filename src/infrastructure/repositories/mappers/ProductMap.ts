import { UniqueEntityID } from 'domain/base'
import Product from 'domain/Product'
import { Price, ProductId, ProductName, ProductQuantity } from 'domain/value-objects'
import { Mapper } from 'infrastructure/core'

export default class ProductMap implements Mapper<Product> {
  toDomain({ name, id, stock, currency, price, giftedWith }: any): Product {
    return new Product({
      name: new ProductName(name),
      id: new UniqueEntityID(id),
      stock: new ProductQuantity(stock),
      giftedWith: new ProductId(giftedWith),
      price: new Price({ currency, amount: price })
    })
  }

  toPersistence(t: Product) {
    return {
      id: t.getId().value,
      name: t.getName().value,
      stock: t.getStock().value,
      price: t.getPrice().value.amount,
      currency: t.getPrice().value.currency.getCode().value,
      giftedWith: t.getGiftedWith()?.value
    }
  }
}
