import Coupon from 'domain/Coupon'
import { UniqueEntityID } from 'domain/base'
import Cart from 'domain/Cart'
import CartEntry from 'domain/CartEntry'
import CartEntryCollection from 'domain/CartEntryCollection'
import { CartRepository, CouponRepository, CurrencyRepository } from 'domain/repositories'
import { CartId, CouponCode, CurrencyCode, ProductQuantity } from 'domain/value-objects'
import { DbConnection, UtilsSQLite } from 'infrastructure/db'
import ProductMap from './mappers/ProductMap'

export default class SQLiteCartRepository implements CartRepository {
  private mapperProduct = new ProductMap()
  private currencyRepository: CurrencyRepository
  private couponRepository: CouponRepository

  constructor(currencyRepo: CurrencyRepository, couponRepo: CouponRepository) {
    this.currencyRepository = currencyRepo
    this.couponRepository = couponRepo
  }

  async findAll(): Promise<Cart[]> {
    const db = await DbConnection.open()

    const cartsQueryResult = await db.all('SELECT * FROM shopping_cart')

    if (!cartsQueryResult || cartsQueryResult.length === 0) {
      throw new Error('No carts found')
    }

    const carts: Cart[] = []
    for (const cartQueryResult of cartsQueryResult) {
      const cart: Cart = await this.parseCart(cartQueryResult)
      await this.idrateCartEntries(cart, cartQueryResult.id, db)
      carts.push(cart)
    }

    db.close()
    return carts
  }

  async getCartById(cartId: CartId): Promise<Cart> {
    const db = await DbConnection.open()

    const cartQueryResult = await db.get('SELECT * FROM shopping_cart WHERE id = ? ', [cartId.value])

    if (!cartQueryResult) {
      throw new Error('Cart not found')
    }
    const cart: Cart = await this.parseCart(cartQueryResult)
    await this.idrateCartEntries(cart, cartQueryResult.id, db)
    db.close()
    return cart
  }

  async save(cart: Cart) {
    try {
      const db = await DbConnection.open()
      await this.deleteEntries(cart.getId(), db)
      await this.upsertCart(cart, db)

      if (cart.getEntries() && cart.getEntries().length > 0) {
        await this.insertCartEntries(cart, db)
      }

      db.close()
    } catch (e) {
      throw new Error(`error inserting cart info from database: ${e}`)
    }
  }

  private async parseCart(cartQueryResult: any): Promise<Cart> {
    const coupon: Coupon = await this.getCartCoupon(cartQueryResult.coupon_id)
    const cart = new Cart({
      id: new UniqueEntityID(cartQueryResult.id),
      currencyBase: await this.currencyRepository.findByCode(new CurrencyCode(cartQueryResult.currency)),
      isComplete: cartQueryResult.is_complete,
      coupon: coupon
    })
    return cart
  }

  private async getCartCoupon(couponId: any): Promise<Coupon> {
    let coupon!: Coupon
    try {
      if (couponId) {
        coupon = await this.couponRepository.findByCode(new CouponCode(couponId))
      }
    } catch (e) {
      console.log(e)
    }
    return coupon
  }

  private async idrateCartEntries(cart: Cart, cartId: any, db: any) {
    const cartEntryCollection: CartEntryCollection = await this.getCartEntries(cartId, db)
    if (cartEntryCollection) {
      cart.setCartEntryCollection(cartEntryCollection)
    }
  }

  async delete(id: CartId) {
    const db = await DbConnection.open()
    await db.get('PRAGMA foreign_keys = ON')
    await db.run('DELETE FROM shopping_cart WHERE id = ? ', [id.value])
    db.close()
  }

  private async insertCartEntries(cart: Cart, db: any) {
    const cartEntries = cart.getEntries()
    const itemsToInsert = cartEntries.map((cartEntry: CartEntry) => ({
      id: cartEntry.getId().value,
      cartId: cart.getId().value,
      productId: cartEntry.getProduct().getId().value,
      quantity: cartEntry.getQuantity().value
    }))
    await UtilsSQLite.insertItemsInBulk(
      'INSERT OR REPLACE INTO cart_entry(id, cart_id, product_id, quantity) VALUES',
      itemsToInsert,
      db
    )
  }

  private async upsertCart(cart: Cart, db: any) {
    await db.run(
      'INSERT OR REPLACE INTO shopping_cart(id, currency, is_complete, total_amount, coupon_id) VALUES(?, ?, ?, ?, ?)',
      [
        cart.getId().value,
        cart.getCurrency().getCode().value,
        cart.getStatus(),
        cart.getTotal()?.value?.amount,
        cart.getCoupon()?.getCode().value
      ]
    )
  }

  private async deleteEntries(cartId: CartId, db: any) {
    db.run('DELETE FROM cart_entry WHERE cart_id = ? ', [cartId.value])
  }

  private async getCartEntries(cartId: string, db: any): Promise<CartEntryCollection> {
    const unformattedCartEntries = await db.all(
      'SELECT ce.id AS product_entry_id, ce.cart_id AS cart_id, ce.product_id AS product_id, ce.quantity AS quantity, pr.stock AS stock, pr.giftedWith AS giftedWith, pr.name AS product_name, pr.price AS product_price, pr.currency AS product_currency FROM cart_entry ce, product pr WHERE cart_id = ? AND ce.product_id = pr.id',
      [cartId]
    )
    const entryList: CartEntry[] = []

    for (const unformattedCartEntry of unformattedCartEntries) {
      const cartEntryCurrency = await this.currencyRepository.findByCode(
        new CurrencyCode(unformattedCartEntry.product_currency)
      )
      entryList.push(
        CartEntry.create(
          this.mapperProduct.toDomain({
            name: unformattedCartEntry.product_name,
            id: unformattedCartEntry.product_id,
            stock: unformattedCartEntry.stock,
            currency: cartEntryCurrency,
            price: unformattedCartEntry.product_price,
            giftedWith: unformattedCartEntry.giftedWith
          }),
          new ProductQuantity(unformattedCartEntry.quantity),
          new UniqueEntityID(unformattedCartEntry.product_entry_id)
        )
      )
    }
    return new CartEntryCollection(entryList)
  }
}
