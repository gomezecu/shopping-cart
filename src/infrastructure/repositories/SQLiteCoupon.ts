import Coupon from 'domain/Coupon'
import { CouponRepository } from 'domain/repositories'
import { CurrencyCode } from 'domain/value-objects'
import { DbConnection } from 'infrastructure/db'
import CouponMap from './mappers/CouponMap'

export default class SQLiteCouponRepository implements CouponRepository {
  private mapper = new CouponMap()

  async findAll(): Promise<Coupon[]> {
    const coupons: Coupon[] = []
    const db = await DbConnection.open()

    const couponsQueryResult = await db.all('SELECT * FROM coupon')
    for (const couponQueryResult of couponsQueryResult) {
      coupons.push(this.mapper.toDomain(couponQueryResult))
    }
    db.close()
    return coupons
  }

  async findByCode(code: CurrencyCode): Promise<Coupon> {
    const db = await DbConnection.open()

    const couponQueryResult = await db.get('SELECT * FROM coupon WHERE code = ?', [code.value])
    const coupon: Coupon = this.mapper.toDomain(couponQueryResult)
    db.close()
    return coupon
  }
}
