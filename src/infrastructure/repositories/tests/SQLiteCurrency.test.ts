import { CurrencyCode } from 'domain/value-objects'
import SQLiteCurrencyRepository from '../SQLiteCurrency'

describe.skip('Repositories', () => {
  describe('Currency', () => {
    it('findAll', async () => {
      const repo = new SQLiteCurrencyRepository()
      const q = await repo.findAll()
      console.log('findAll', q)
    })

    it('getBase', async () => {
      const repo = new SQLiteCurrencyRepository()
      const q = await repo.getBase()
      console.log('getBase', q)
    })

    it('findByCode', async () => {
      const repo = new SQLiteCurrencyRepository()
      const q = await repo.findByCode(new CurrencyCode('USD'))
      console.log('findByCode', q)
    })
  })
})
