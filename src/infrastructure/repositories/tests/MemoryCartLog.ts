import CartLogEvent from 'domain/CartLogEvent'
import { CartLogRepository } from 'domain/repositories'

export default class MemoryCartLogRepository implements CartLogRepository {
  saveEvent(cartLogEvent: CartLogEvent): void {
    console.log('MemoryCartLogRepository cartLogEvent: ', cartLogEvent)
  }
}
