import { ProductId } from 'domain/value-objects'
import SQLiteProductRepository from '../SQLiteProduct'
import { generateProducts } from './MemoryProduct'

describe.skip('Repositories', () => {
  describe('Product', () => {
    it('save', async () => {
      const product = generateProducts()
      const repo = new SQLiteProductRepository()
      const q = await repo.save(product[0])
      console.log('save', q)
    })

    it('get', async () => {
      const repo = new SQLiteProductRepository()
      const q = await repo.get(new ProductId('P-000001'))
      console.log('q', q)
    })

    it.only('saveCollection', async () => {
      const product = generateProducts(3, 5, 17.2, 'pro')
      const repo = new SQLiteProductRepository()
      const q = await repo.saveCollection(product)
      console.log('q', q)
    })
  })
})
