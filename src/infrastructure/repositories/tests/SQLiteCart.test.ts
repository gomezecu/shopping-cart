import Currency from 'domain/Currency'
import Cart from 'domain/Cart'
import { CartId, CouponCode, CurrencyCode, CurrencyName, ProductId, ProductQuantity } from 'domain/value-objects'
import { SQLiteCouponRepository, SQLiteCurrencyRepository, SQLiteProductRepository } from '..'
import SQLiteCart from '../SQLiteCart'

describe.skip('Repositories', () => {
  describe('Shopping Cart', () => {
    it('Should get a cart', async () => {
      const currencyRepo = new SQLiteCurrencyRepository()
      const couponRepo = new SQLiteCouponRepository()
      const repo: SQLiteCart = new SQLiteCart(currencyRepo, couponRepo)
      const cart: Cart = await repo.getCartById(new CartId('asdlfjlksdajfqo2ie-askdfj'))
      expect(cart)
      expect(cart.getTotal().value.amount === 12)
      expect(cart.getTotal().value.currency.getCode().value === 'EUR')
      expect(cart.getStatus())
    })

    it('should save a cart without entries', async () => {
      const currencyRepo = new SQLiteCurrencyRepository()
      const couponRepo = new SQLiteCouponRepository()
      const repo: SQLiteCart = new SQLiteCart(currencyRepo, couponRepo)
      const currencyTemp = new Currency({
        code: new CurrencyCode('EUR'),
        name: new CurrencyName('Euro'),
        isBase: true
      })
      const cart = new Cart({ id: new CartId('cart1'), currencyBase: currencyTemp })
      repo.save(cart)
    })

    it('should save a cart with entries', async () => {
      const currencyRepo = new SQLiteCurrencyRepository()
      const couponRepo = new SQLiteCouponRepository()
      const productRepo = new SQLiteProductRepository()
      const repo: SQLiteCart = new SQLiteCart(currencyRepo, couponRepo)
      const currencyTemp = new Currency({
        code: new CurrencyCode('EUR'),
        name: new CurrencyName('Euro'),
        isBase: true
      })
      const cart = new Cart({ id: new CartId('cart1'), currencyBase: currencyTemp })
      const product = await productRepo.get(new ProductId('P-000001'))
      cart.addProduct(product, new ProductQuantity(4))
      console.log(cart)
      await repo.save(cart)
    })

    it('should save a cart with multiple entries and coupon', async () => {
      const currencyRepo = new SQLiteCurrencyRepository()
      const couponRepo = new SQLiteCouponRepository()
      const productRepo = new SQLiteProductRepository()
      const repo: SQLiteCart = new SQLiteCart(currencyRepo, couponRepo)
      const currencyTemp = new Currency({
        code: new CurrencyCode('EUR'),
        name: new CurrencyName('Euro'),
        isBase: true
      })
      const cart = new Cart({ id: new CartId('cart2'), currencyBase: currencyTemp })
      const product = await productRepo.get(new ProductId('P-000001'))
      const product2 = await productRepo.get(new ProductId('P-000002'))
      cart.addProduct(product, new ProductQuantity(4))
      cart.addProduct(product2, new ProductQuantity(1))
      const coupon = await couponRepo.findByCode(new CouponCode('PERCENT_50'))
      cart.applyCoupon(coupon)
      await repo.save(cart)
    })

    it('should delete the cart', async () => {
      const currencyRepo = new SQLiteCurrencyRepository()
      const couponRepo = new SQLiteCouponRepository()
      const repo: SQLiteCart = new SQLiteCart(currencyRepo, couponRepo)
      await repo.delete(new CartId('asdlfjlksdajfqo2ie-askdfj'))
    })

    it('should should return all carts', async () => {
      const currencyRepo = new SQLiteCurrencyRepository()
      const couponRepo = new SQLiteCouponRepository()
      const repo: SQLiteCart = new SQLiteCart(currencyRepo, couponRepo)
      const carts: Cart[] = await repo.findAll()
      console.log(carts)
      console.log(carts[0].getEntries())
    })
  })
})
