import CouponFixed from 'domain/CouponFixed'
import CouponPercent from 'domain/CouponPercent'
import { CouponRepository } from 'domain/repositories'
import { CouponAmount, CouponCode, CouponMinTotal } from 'domain/value-objects'

export default class MemoryCouponRepository implements CouponRepository {
  private coupons = [
    new CouponFixed({
      code: new CouponCode('FIXED_10'),
      discount: new CouponAmount(10),
      minTotal: new CouponMinTotal(10),
      isActive: true
    }),
    new CouponPercent({
      code: new CouponCode('PERCENT_50'),
      discount: new CouponAmount(50),
      minTotal: new CouponMinTotal(15),
      isActive: true
    }),
    new CouponPercent({
      code: new CouponCode('PERCENT_10'),
      discount: new CouponAmount(10),
      isActive: true
    })
  ]

  async findByCode(code: CouponCode) {
    const coupon = this.coupons.find((item) => item.getCode().value === code.value)
    if (!coupon) {
      throw new Error('Coupon not found')
    }
    return coupon
  }

  async findAll() {
    return this.coupons
  }
}
