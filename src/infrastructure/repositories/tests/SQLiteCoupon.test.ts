import { CurrencyCode } from 'domain/value-objects'
import SQLiteCouponRepository from '../SQLiteCoupon'

describe.skip('Repositories', () => {
  describe('Coupon', () => {
    it('Should return all coupons', async () => {
      const repo = new SQLiteCouponRepository()
      const q1 = await repo.findAll()
      expect(q1.length > 0)
    })
  })

  it('should return a single coupon', async () => {
    const repo = new SQLiteCouponRepository()
    const q1 = await repo.findByCode(new CurrencyCode('FIXED_10'))
    expect(q1)
    expect(q1.getCode().value === 'FIXED_10')
  })
})
