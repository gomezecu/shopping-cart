import Currency from 'domain/Currency'
import { CurrencyRepository } from 'domain/repositories'
import { CurrencyCode, CurrencyName } from 'domain/value-objects'

export default class MemoryCurrencyRepository implements CurrencyRepository {
  private currencies = [
    new Currency({
      code: new CurrencyCode('EUR'),
      name: new CurrencyName('Euro'),
      isBase: true
    }),
    new Currency({
      code: new CurrencyCode('USD'),
      name: new CurrencyName('Dollar'),
      isBase: false
    }),
    new Currency({
      code: new CurrencyCode('GBP'),
      name: new CurrencyName('Libra'),
      isBase: false
    })
  ]

  async getBase() {
    const base = this.currencies.find((item) => item.getIsBase())
    if (!base) {
      throw new Error('Base currency invalid')
    }
    return base
  }

  async findByCode(code: CurrencyCode) {
    const currency = this.currencies.find((item) => item.getCode().value === code.value)
    if (!currency) {
      throw new Error('Currency not found')
    }
    return currency
  }

  async findAll() {
    return this.currencies
  }
}
