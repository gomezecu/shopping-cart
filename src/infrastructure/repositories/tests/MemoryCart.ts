import Cart from 'domain/Cart'
import Currency from 'domain/Currency'
import { CartRepository } from 'domain/repositories'
import { CartId, CurrencyCode, CurrencyName } from 'domain/value-objects'

const currencyTemp = new Currency({
  code: new CurrencyCode('EUR'),
  name: new CurrencyName('Euro'),
  isBase: true
})

export default class MemoryCartRepository implements CartRepository {
  private carts = [
    new Cart({ id: new CartId('cart1'), currencyBase: currencyTemp }),
    new Cart({ id: new CartId('cart2'), currencyBase: currencyTemp })
  ]

  getCartById(id: CartId): Promise<Cart> {
    const cart = this.checkExistence(id)
    return Promise.resolve(cart)
  }

  findAll(): Promise<Cart[]> {
    return Promise.resolve(Object.values(this.carts))
  }

  save(cart: Cart) {
    let cartAux = null
    this.carts.forEach((item) => {
      if (item.getId().value === cart.getId().value) {
        item = cart
        cartAux = cart
      }
    })
    if (!cartAux) {
      this.carts.push(cart)
    }
  }

  delete(id: CartId) {
    this.checkExistence(id)
    this.carts = this.carts.filter((item) => item.getId().value !== id.value)
  }

  private checkExistence(id: CartId) {
    const cart = this.carts.find((item) => item.getId().value === id.value)
    if (!cart) {
      throw new Error('Cart not found')
    }
    return cart
  }
}
