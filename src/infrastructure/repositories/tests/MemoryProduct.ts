import Currency from 'domain/Currency'
import Product from 'domain/Product'
import { ProductRepository } from 'domain/repositories'
import { CurrencyCode, CurrencyName, Price, ProductId, ProductName, ProductQuantity } from 'domain/value-objects'

export const generateProducts = (n = 12, stock = 5, price = 7, namePrefix = 'id') =>
  Array.from(Array(n).keys()).map(
    (item, index) =>
      new Product({
        name: new ProductName('product' + index),
        id: new ProductId(namePrefix + index),
        stock: new ProductQuantity(stock),
        price: new Price({
          amount: price,
          currency: new Currency({
            code: new CurrencyCode('EUR'),
            name: new CurrencyName('Euro'),
            isBase: true
          })
        }),
        giftedWith: index === 2 ? new ProductId('id1') : undefined
      })
  )

export default class MemoryProductRepository implements ProductRepository {
  private list: Product[]

  constructor(products: Product[] = generateProducts()) {
    this.list = products
  }

  get(productId: ProductId): Promise<Product> {
    const product = this.list.find((item) => item.getId().value === productId.value)
    if (!product) {
      throw new Error('Product not found')
    }
    return Promise.resolve(product)
  }

  save(product: Product) {
    this.list.push(product)
  }

  saveCollection(products: Product[]) {
    products.forEach((product) => {
      this.list.forEach((item, index) => {
        if (item.getId().value === product.getId().value) {
          this.list[index] = item
        }
      })
    })
  }
}
