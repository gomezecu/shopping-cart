import CartLogEvent from 'domain/CartLogEvent'
import SQLiteCartLog from '../SQLiteCartLog'

describe.skip('Repositories', () => {
  describe('Shopping Cart Log', () => {
    it('Should save the event without errors', async () => {
      const repo = new SQLiteCartLog()
      const logevent: CartLogEvent = new CartLogEvent(
        'create card',
        new Date(),
        'asldjfasl-asdjl',
        '{cart:{cartId: "asldjfasl-asdjl"}}'
      )

      const query = await repo.saveEvent(logevent)
      expect(query)
      expect(query.changes === 1)
      console.log(query)
    })
  })
})
