import Currency from 'domain/Currency'
import { CurrencyRepository } from 'domain/repositories'
import { CurrencyCode } from 'domain/value-objects'
import DbConnection from 'infrastructure/db/config'
import CurrencyMap from './mappers/CurrencyMap'

export default class SQLiteCurrencyRepository implements CurrencyRepository {
  mapper = new CurrencyMap()
  async getBase(): Promise<Currency> {
    const db = await DbConnection.open()

    const result = await db.get('SELECT * FROM currency WHERE is_base = true')
    if (!result) {
      throw new Error('Base currency invalid')
    }

    db.close()

    return this.mapper.toDomain(result)
  }

  async findAll(): Promise<Currency[]> {
    const db = await DbConnection.open()

    const result = await db.all('SELECT * FROM currency')
    if (!result) {
      throw new Error('No currencies found')
    }

    const currencies: Currency[] = []
    for (const currency of result) {
      currencies.push(this.mapper.toDomain(currency))
    }
    db.close()
    return currencies
  }

  async findByCode(code: CurrencyCode): Promise<Currency> {
    const db = await DbConnection.open()

    const result = await db.get('SELECT * FROM currency WHERE code = ?', [code.value])

    if (!result) {
      throw new Error('Currency not found')
    }

    const baseCurrency = this.mapper.toDomain(result)
    db.close()
    return baseCurrency
  }
}
