import Product from 'domain/Product'
import { ProductRepository } from 'domain/repositories'
import { ProductId } from 'domain/value-objects'
import { UtilsSQLite } from 'infrastructure/db'
import DbConnection from 'infrastructure/db/config'
import ProductMap from './mappers/ProductMap'

export default class SQLiteProductRepository implements ProductRepository {
  mapper = new ProductMap()

  async get(id: ProductId): Promise<Product> {
    const db = await DbConnection.open()
    const productQueryResult = await db.get('SELECT * FROM product WHERE id = ?', [id.value])

    if (!productQueryResult) {
      throw new Error('Product not found')
    }

    db.close()

    return this.mapper.toDomain(productQueryResult)
  }

  async save(product: Product) {
    const db = await DbConnection.open()
    await db.run(
      'INSERT INTO product(id, name, stock, price, currency, giftedWith) VALUES(?, ?, ?, ?, ?, ?)',
      Object.values(this.mapper.toPersistence(product))
    )
    db.close()
  }

  async saveCollection(product: Product[]) {
    const db = await DbConnection.open()
    const productValueList = product.map((item) => this.mapper.toPersistence(item))
    await UtilsSQLite.insertItemsInBulk(
      'INSERT OR REPLACE INTO product(id, name, stock, price, currency) VALUES',
      productValueList,
      db
    )

    db.close()
  }
}
