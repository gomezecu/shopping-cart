import axios from 'axios'
import Currency from 'domain/Currency'
import { ExchangeRateRepository } from 'domain/repositories'
import { Price } from 'domain/value-objects'

export default class FixerExchangeRateRepository implements ExchangeRateRepository {
  private baseUrl = 'http://data.fixer.io/api/'
  // TODO: hide
  private apiKey = '8e8853e556da9fe73eedafbc301f6e2b'

  async convert(from: Price, to: Currency): Promise<Price> {
    try {
      const response = await axios.get(
        `${this.baseUrl}latest?access_key=${this.apiKey}&base=${from.value.currency.getCode().value}&symbols=${
          to.getCode().value
        }`
      )
      const { data } = response

      if (!data.success) {
        throw new Error('Error using Fixer')
      }

      return new Price({
        amount: parseFloat((data.rates[to.getCode().value] * from.value.amount).toFixed(2)),
        currency: to
      })
    } catch (err) {
      throw new Error(`Something happened with Fixer: ${err}`)
    }
  }
}
