import sqlite3 from 'sqlite3'
import { open } from 'sqlite'
require('dotenv').config()

const { DB_NAME } = process.env

export default class DbConnection {
  static async open() {
    return await open({
      filename: DB_NAME!!,
      mode: sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE,
      driver: sqlite3.Database
    })
  }
}
