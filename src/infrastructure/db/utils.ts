export default class UtilsSQLite {
  static async insertItemsInBulk(query: String, itemList: Array<any>, db: any) {
    let placeholder = ''
    itemList.forEach((item, index) => {
      placeholder =
        placeholder +
        ' (' +
        Object.values(item)
          .map((item) => (typeof item === 'string' ? "'" + item + "'" : item))
          .join(',') +
        ')' +
        (index === itemList.length - 1 ? '' : ',')
    })
    const sql = `${query}${placeholder}`
    await db.run(sql)
  }
}
