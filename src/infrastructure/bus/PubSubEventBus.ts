import { DomainEvent, DomainEventSubscriber, EventBus } from 'domain/base'
import PubSub from 'pubsub-js'

export class PubSubEventBus implements EventBus {
  constructor(subscribers: Array<DomainEventSubscriber<DomainEvent>>) {
    subscribers.forEach((subscriber) => {
      this.registerOnEventBus(subscriber)
    })
  }

  publish(events: DomainEvent[]): void {
    events.forEach((event) => {
      this.publishEvent(event)
    })
  }

  private publishEvent(event: DomainEvent) {
    const eventIdentifier = event.action
    PubSub.publish(eventIdentifier, event)
  }

  private registerOnEventBus(subscriber: DomainEventSubscriber<DomainEvent>) {
    const eventIdentifier = subscriber.subscribeTo()
    PubSub.subscribe(eventIdentifier, this.eventConsumer(subscriber))
  }

  private eventConsumer(subscriber: DomainEventSubscriber<DomainEvent>) {
    return (msg: string, data: any) => {
      subscriber.consume(data as any)
    }
  }
}
