import { EventBus, DomainEvent } from 'domain/base'

export class MockEventBus implements EventBus {
  publish(events: DomainEvent[]): void {}
}
