import CheckoutCart from 'application/CheckoutCart'
import { CheckoutCartCommand } from 'application/commands'
import { Request, Response } from 'express'
import { BaseController } from '../models/BaseController'

export class CheckoutCartController extends BaseController {
  private useCase: CheckoutCart

  constructor(useCase: CheckoutCart) {
    super()
    this.useCase = useCase
  }

  async executeImpl(req: Request, res: Response): Promise<any> {
    try {
      const { cartId } = req.body
      await this.useCase.execute(new CheckoutCartCommand(cartId))
      return this.ok(res)
    } catch (err) {
      return this.fail(res, err as any)
    }
  }
}
