import ApplyCouponCart from 'application/ApplyCouponCart'
import { ApplyCouponCartCommand } from 'application/commands'
import { Request, Response } from 'express'
import { BaseController } from '../models/BaseController'

export class ApplyCouponCartController extends BaseController {
  private useCase: ApplyCouponCart

  constructor(useCase: ApplyCouponCart) {
    super()
    this.useCase = useCase
  }

  async executeImpl(req: Request, res: Response): Promise<any> {
    try {
      const { cartId, couponCode } = req.body
      await this.useCase.execute(new ApplyCouponCartCommand(cartId, couponCode))
      return this.ok(res)
    } catch (err) {
      return this.fail(res, err as any)
    }
  }
}
