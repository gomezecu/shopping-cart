import CreateCart from 'application/CreateCart'
import { Request, Response } from 'express'
import { BaseController } from '../models/BaseController'

export class CreateCartController extends BaseController {
  private useCase: CreateCart

  constructor(useCase: CreateCart) {
    super()
    this.useCase = useCase
  }

  async executeImpl(req: Request, res: Response): Promise<any> {
    try {
      const data = await this.useCase.execute()
      return this.ok(res, data)
    } catch (err) {
      return this.fail(res, err as any)
    }
  }
}
