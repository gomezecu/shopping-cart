import { GetCart } from 'application'
import { GetCartQuery } from 'application/queries'
import { Request, Response } from 'express'
import { BaseController } from '../models/BaseController'

export class GetCartController extends BaseController {
  private useCase: GetCart

  constructor(useCase: GetCart) {
    super()
    this.useCase = useCase
  }

  async executeImpl(req: Request, res: Response): Promise<any> {
    try {
      const { cartId, currencyCode } = req.body
      const data = await this.useCase.execute(new GetCartQuery(cartId, currencyCode))
      return this.ok(res, data)
    } catch (err) {
      return this.fail(res, err as any)
    }
  }
}
