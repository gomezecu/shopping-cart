import { AddProductCart, GetCart, RemoveCartProduct } from 'application'
import { AddProductCartController } from './AddProductCartController'
import { RemoveCartProductController } from './RemoveCartProductController'
import { GetCartController } from './GetCartController'
import { ApplyCouponCartController } from './ApplyCouponCartController'
import ApplyCouponCart from 'application/ApplyCouponCart'
import { PubSubEventBus } from 'infrastructure/bus/PubSubEventBus'
import { CheckoutCartController } from './CheckoutCartController'
import CheckoutCart from 'application/CheckoutCart'
import { ListCartController } from './ListCartController'
import ListCarts from 'application/ListCarts'
import { CreateCartController } from './CreateCartController'
import CreateCart from 'application/CreateCart'
import {
  MemoryCartLogRepository,
  MemoryCartRepository,
  MemoryCouponRepository,
  MemoryCurrencyRepository,
  MemoryProductRepository
} from 'infrastructure/repositories/tests'
import {
  CartLogAddProductSubscriber,
  CartLogApplyCouponSubscriber,
  CartLogCheckoutSubscriber,
  CartLogRemoveProductSubscriber
} from 'application/subscribers'
import {
  FixerExchangeRateRepository,
  SQLiteCartLogRepository,
  SQLiteCartRepository,
  SQLiteCouponRepository,
  SQLiteCurrencyRepository,
  SQLiteProductRepository
} from 'infrastructure/repositories'
import { CreateCartDetail } from 'domain/services'

const memoryCurrencyRepository = new SQLiteCurrencyRepository()
const fixerExchangeRateRepository = new FixerExchangeRateRepository()
const memoryProductRepository = new SQLiteProductRepository()
const memoryCouponRepository = new SQLiteCouponRepository()
const memoryCartLogRepository = new SQLiteCartLogRepository()
const memoryCartRepository = new SQLiteCartRepository(memoryCurrencyRepository, memoryCouponRepository)
const pubSubEventBus = new PubSubEventBus([
  new CartLogAddProductSubscriber(memoryCartLogRepository),
  new CartLogRemoveProductSubscriber(memoryCartLogRepository),
  new CartLogApplyCouponSubscriber(memoryCartLogRepository),
  new CartLogCheckoutSubscriber(memoryCartLogRepository)
])

const addProductCartController = new AddProductCartController(
  new AddProductCart(memoryCartRepository, memoryProductRepository, pubSubEventBus)
)
const deleteCartEntryByIdController = new RemoveCartProductController(
  new RemoveCartProduct(memoryCartRepository, pubSubEventBus)
)

const getCartController = new GetCartController(
  new GetCart(memoryCartRepository, fixerExchangeRateRepository, memoryCurrencyRepository, new CreateCartDetail())
)

const listCartController = new ListCartController(new ListCarts(memoryCartRepository, new CreateCartDetail()))

const applyCouponCartController = new ApplyCouponCartController(
  new ApplyCouponCart(memoryCartRepository, memoryCouponRepository, pubSubEventBus)
)

const checkoutCartController = new CheckoutCartController(
  new CheckoutCart(memoryCartRepository, memoryProductRepository, pubSubEventBus)
)

const createCartController = new CreateCartController(
  new CreateCart(memoryCartRepository, new MemoryCurrencyRepository(), new CreateCartDetail())
)

export {
  addProductCartController,
  deleteCartEntryByIdController,
  getCartController,
  applyCouponCartController,
  checkoutCartController,
  listCartController,
  createCartController
}
