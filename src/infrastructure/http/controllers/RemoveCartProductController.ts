import { RemoveCartProductCommand } from 'application/commands'
import RemoveCartProduct from 'application/RemoveCartProduct'
import { Request, Response } from 'express'
import { BaseController } from '../models/BaseController'

export class RemoveCartProductController extends BaseController {
  private useCase: RemoveCartProduct

  constructor(useCase: RemoveCartProduct) {
    super()
    this.useCase = useCase
  }

  async executeImpl(req: Request, res: Response): Promise<any> {
    try {
      const { cartId, productId } = req.body
      await this.useCase.execute(new RemoveCartProductCommand(cartId, productId))
      return this.ok(res)
    } catch (err) {
      return this.fail(res, err as any)
    }
  }
}
