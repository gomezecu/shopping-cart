import { AddProductCart } from 'application'
import { AddProductCartCommand } from 'application/commands'
import { Request, Response } from 'express'
import { BaseController } from '../models/BaseController'

export class AddProductCartController extends BaseController {
  private useCase: AddProductCart

  constructor(useCase: AddProductCart) {
    super()
    this.useCase = useCase
  }

  async executeImpl(req: Request, res: Response): Promise<any> {
    try {
      const { cartId, productId, quantity } = req.body
      await this.useCase.execute(new AddProductCartCommand(cartId, productId, quantity))
      return this.ok(res)
    } catch (err) {
      return this.fail(res, err as any)
    }
  }
}
