import express from 'express'
import { cartRouter } from './routes'

const v1Router = express.Router()

v1Router.get('/', (req, res) => {
  return res.json({ message: "Yo! we're up" })
})

v1Router.use('/cart', cartRouter)

export { v1Router }
