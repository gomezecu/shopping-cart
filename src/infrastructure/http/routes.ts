import express from 'express'
import {
  addProductCartController,
  deleteCartEntryByIdController,
  getCartController,
  applyCouponCartController,
  checkoutCartController,
  listCartController,
  createCartController
} from './controllers'
const cartRouter = express.Router()

cartRouter.post('/product', (req, res) => addProductCartController.execute(req, res))

cartRouter.delete('/product', (req, res) => deleteCartEntryByIdController.execute(req, res))

cartRouter.get('/', (req, res) => getCartController.execute(req, res))

cartRouter.post('/', (req, res) => createCartController.execute(req, res))

cartRouter.get('/list', (req, res) => listCartController.execute(req, res))

cartRouter.post('/checkout', (req, res) => checkoutCartController.execute(req, res))

cartRouter.post('/coupon', (req, res) => applyCouponCartController.execute(req, res))

export { cartRouter }
