import Cart from '../Cart'
import { CartId } from '../value-objects'

export default interface CartRepository {
  getCartById(cartId: CartId): Promise<Cart>
  findAll(): Promise<Cart[]>
  save(cart: Cart): void
  delete(id: CartId): void
}
