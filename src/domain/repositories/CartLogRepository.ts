import CartLogEvent from 'domain/CartLogEvent'

export default interface CartLogRepository {
  saveEvent(cartLogEvent: CartLogEvent): void
}
