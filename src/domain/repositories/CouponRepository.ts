import Coupon from 'domain/Coupon'
import CurrencyCode from 'domain/value-objects/CurrencyCode'

export default interface CouponRepository {
  findAll(): Promise<Coupon[]>
  findByCode(code: CurrencyCode): Promise<Coupon>
}
