import Currency from 'domain/Currency'
import CurrencyCode from 'domain/value-objects/CurrencyCode'

export default interface CurrencyRepository {
  getBase(): Promise<Currency>
  findAll(): Promise<Currency[]>
  findByCode(code: CurrencyCode): Promise<Currency>
}
