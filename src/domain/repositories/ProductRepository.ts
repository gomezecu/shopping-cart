import Product from 'domain/Product'
import { ProductId } from 'domain/value-objects'

export default interface ProductRepository {
  get(id: ProductId): Promise<Product>
  save(product: Product): void
  saveCollection(product: Product[]): void
}
