import Currency from 'domain/Currency'
import { Price } from 'domain/value-objects'

export default interface ExchangeRateRepository {
  convert(from: Price, to: Currency): Promise<Price>
}
