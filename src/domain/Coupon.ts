import { Entity, UniqueEntityID } from './base'
import { CouponAmount, CouponCode, CouponMinTotal, Price } from './value-objects'

export interface CouponProps {
  discount: CouponAmount
  id?: UniqueEntityID
  minTotal?: CouponMinTotal
  code: CouponCode
  isActive: boolean
}

export default abstract class Coupon extends Entity {
  protected discount: CouponAmount
  protected minTotal?: CouponMinTotal
  protected isActive?: boolean
  protected code: CouponCode

  constructor({ id, discount, minTotal, code, isActive = true }: CouponProps) {
    super(id)
    this.discount = discount
    this.minTotal = minTotal
    this.code = code
    this.isActive = isActive
  }

  getCode() {
    return this.code
  }

  markAsUnactive() {
    this.isActive = false
  }

  calculate(total: number): number {
    throw new Error('Method not implemented.')
  }

  valid(total: Price) {
    if (this.isActive && (this.minTotal ? total.value.amount >= this.minTotal.value : true)) {
      return
    }
    throw new Error('Coupon invalid')
  }
}
