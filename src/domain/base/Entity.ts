import UniqueEntityID from './UniqueEntityID'

export default abstract class Entity {
  protected readonly _id: UniqueEntityID

  constructor(id?: UniqueEntityID) {
    this._id = id || new UniqueEntityID()
  }

  getId() {
    return this._id
  }
}
