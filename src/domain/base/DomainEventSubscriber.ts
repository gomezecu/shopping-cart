import DomainEvent from './DomainEvent'

export default interface DomainEventSubscriber<EventType extends DomainEvent> {
  consume(event: EventType): void
  subscribeTo(): string
}
