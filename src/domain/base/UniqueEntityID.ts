import { generateUUID } from 'shared/utils/generateUUID'
import ValueObject from './ValueObject'

export default class UniqueEntityID extends ValueObject<string> {
  public readonly value: string
  constructor(id?: string) {
    super()
    this.value = id ?? generateUUID()
  }
}
