export default abstract class ValueObject<T> {
  public abstract readonly value: T
}
