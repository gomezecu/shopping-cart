export default interface DomainEvent {
  action: string
  date: Date
  cartId: string
}
