import Entity from './Entity'
import DomainEvent from './DomainEvent'
import UniqueEntityID from './UniqueEntityID'

export default abstract class AggregateRoot extends Entity {
  private recordedDomainEvents: Array<DomainEvent> = []

  public pullDomainEvents(): Array<DomainEvent> {
    const recordedDomainEvents = [...this.recordedDomainEvents]
    this.recordedDomainEvents = []

    return recordedDomainEvents
  }

  protected record(event: DomainEvent) {
    this.recordedDomainEvents.push(event)
  }

  getId(): UniqueEntityID {
    return this._id
  }
}
