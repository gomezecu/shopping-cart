import Product from './Product'
import CartEntry from './CartEntry'
import Coupon from './Coupon'
import Currency from './Currency'
import { AddProductCartEvent, ApplyCouponCartEvent, CheckoutCartEvent, RemoveCartProductEvent } from './events'
import { AggregateRoot, UniqueEntityID } from './base'
import { Price, ProductId, ProductQuantity } from './value-objects'
import { CartEntryCollection } from './index'

interface CartProps {
  id?: UniqueEntityID
  currencyBase: Currency
  isComplete?: boolean
  coupon?: Coupon
}

export default class Cart extends AggregateRoot {
  private entries: CartEntryCollection
  private currencyBase: Currency
  private coupon?: Coupon
  private isComplete = false

  constructor({ id, currencyBase, isComplete = false, coupon = undefined }: CartProps) {
    super(id)
    this.entries = new CartEntryCollection([])
    this.currencyBase = currencyBase
    this.isComplete = isComplete
    this.coupon = coupon
  }

  getEntries() {
    return this.entries.getEntries()
  }

  getCurrency() {
    return this.currencyBase
  }

  getStatus() {
    return this.isComplete
  }

  addProduct(product: Product, quantity: ProductQuantity) {
    const cartLogAddProduct = () => {
      const addProductEvent = new AddProductCartEvent(this.getId().value, product.getId().value, quantity.value)
      this.record(addProductEvent)
    }
    const entry = this.entries.findByProductId(product.getId())
    if (entry) {
      cartLogAddProduct()
      return entry.add(quantity)
    }

    const newCartEntry = CartEntry.create(product, quantity)
    this.entries.addEntry(newCartEntry)
    cartLogAddProduct()
  }

  removeProduct(productId: ProductId) {
    this.entries.remove(productId)

    const removeProductEvent = new RemoveCartProductEvent(this.getId().value, productId.value)
    this.record(removeProductEvent)
  }

  checkout() {
    this.isComplete = true

    const checkoutEvent = new CheckoutCartEvent(this.getId().value)
    this.record(checkoutEvent)
  }

  applyCoupon(coupon: Coupon) {
    if (this.getEntries().length === 0) {
      throw new Error('Cart empty')
    }
    coupon.valid(this.getTotal())
    this.coupon = coupon

    const applyCouponEvent = new ApplyCouponCartEvent(this.getId().value, coupon.getCode().value)
    this.record(applyCouponEvent)
  }

  setCartEntryCollection(entryCollection: CartEntryCollection) {
    this.entries = entryCollection
  }

  getCoupon() {
    return this.coupon
  }

  getTotal(): Price {
    if (!this.coupon) {
      return new Price({
        amount: this.entries.getTotal(),
        currency: this.currencyBase
      })
    }

    return new Price({
      amount: this.coupon.calculate(this.entries.getTotal()),
      currency: this.currencyBase
    })
  }
}
