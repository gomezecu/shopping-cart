import Coupon, { CouponProps } from './Coupon'

export default class CouponPercent extends Coupon {
  constructor({ id, discount, code, minTotal, isActive }: CouponProps) {
    super({ id, discount, code, minTotal, isActive })
  }

  calculate(total: number) {
    return total * (this.discount.value / 100)
  }
}
