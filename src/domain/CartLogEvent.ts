import { DomainEvent } from './base'

export default class CartLogEvent implements DomainEvent {
  action: string
  date: Date
  cartId: string
  details: string

  constructor(action: string, date: Date, cartId: string, details: string) {
    this.action = action
    this.date = date
    this.cartId = cartId
    this.details = details
  }
}
