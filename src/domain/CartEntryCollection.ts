import CartEntry from './CartEntry'
import { ProductId } from './value-objects'

export default class CartEntryCollection {
  private entries: CartEntry[]
  private static validationdDifferentsProduct = {
    quantity: 10,
    message: 'Cart full of products'
  }

  constructor(entries: CartEntry[]) {
    this.entries = entries
  }

  public getEntries() {
    return this.entries
  }

  public remove(productId: ProductId) {
    const cartEntry = this.findByProductId(productId)

    if (!cartEntry) {
      throw new Error('Product not in cart')
    }

    this.entries = this.entries.filter((entry) => {
      const product = entry.getProduct()
      const matchMainCartEntry = product.getId().value === productId.value
      const matchGiftCartEntry = product.getGiftedWith()?.value === productId.value

      return !matchMainCartEntry && !matchGiftCartEntry
    })
  }

  public addEntry(entry: CartEntry) {
    if (this.isFullDifferentProducts()) {
      throw new Error(CartEntryCollection.validationdDifferentsProduct.message)
    }
    entry.getProduct().hasStock(entry.getQuantity())
    this.entries.push(entry)
  }

  public findByProductId(productId: ProductId) {
    return this.entries.find((item) => item.getProduct().getId().value === productId.value)
  }

  public getTotal() {
    return this.entries.reduce((acc, entry) => {
      const productGiftedWith = entry.getProduct().getGiftedWith()

      if (productGiftedWith) {
        const parentProduct = this.findByProductId(productGiftedWith)

        if (parentProduct) return acc
      }

      return acc + entry.getTotal()
    }, 0)
  }

  private isFullDifferentProducts() {
    return CartEntryCollection.validationdDifferentsProduct.quantity <= this.entries.length
  }
}
