import { Entity, UniqueEntityID } from './base'
import { Price, ProductId, ProductName, ProductQuantity } from './value-objects'

interface ProductProps {
  id?: UniqueEntityID
  name: ProductName
  stock: ProductQuantity
  price: Price
  giftedWith?: ProductId
}

export default class Product extends Entity {
  private name: ProductName
  private stock: ProductQuantity
  private price: Price
  private giftedWith?: ProductId

  constructor({ name, id, stock, price, giftedWith }: ProductProps) {
    super(id)
    this.name = name
    this.stock = stock
    this.price = price
    this.giftedWith = giftedWith
  }

  getPrice() {
    return this.price
  }

  getStock() {
    return this.stock
  }

  setStock(stock: ProductQuantity) {
    this.stock = stock
  }

  getName() {
    return this.name
  }

  getGiftedWith() {
    return this.giftedWith
  }

  hasStock(quantity: ProductQuantity) {
    if (!(this.stock.value > 0 && this.stock.value >= quantity.value)) {
      throw new Error('Product without stock')
    }
  }
}
