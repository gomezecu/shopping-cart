import { DomainEvent } from 'domain/base'

export default class ApplyCouponCart implements DomainEvent {
  action: string
  date: Date
  cartId: string
  couponCode: string

  constructor(cartId: string, couponCode: string) {
    this.action = ApplyCouponCart.name
    this.date = new Date()
    this.cartId = cartId
    this.couponCode = couponCode
  }

  public static create(cartId: string, couponCode: string) {
    return new ApplyCouponCart(cartId, couponCode)
  }
}
