import { DomainEvent } from 'domain/base'

export default class RemoveCartProduct implements DomainEvent {
  action: string
  date: Date
  cartId: string
  productId: string

  constructor(cartId: string, productId: string) {
    this.action = RemoveCartProduct.name
    this.date = new Date()
    this.cartId = cartId
    this.productId = productId
  }

  public static create(cartId: string, productId: string) {
    return new RemoveCartProduct(cartId, productId)
  }
}
