import { DomainEvent } from 'domain/base'

export default class CheckoutCart implements DomainEvent {
  action: string
  date: Date
  cartId: string

  constructor(cartId: string) {
    this.action = CheckoutCart.name
    this.date = new Date()
    this.cartId = cartId
  }
}
