import DomainEvent from 'domain/base/DomainEvent'

export default class AddProductCart implements DomainEvent {
  action: string
  date: Date
  cartId: string
  productId: string
  quantity: number

  constructor(cartId: string, productId: string, quantity: number) {
    this.action = AddProductCart.name
    this.date = new Date()
    this.cartId = cartId
    this.productId = productId
    this.quantity = quantity
  }
}
