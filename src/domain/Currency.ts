import { Entity, UniqueEntityID } from './base'
import { CurrencyCode, CurrencyName } from './value-objects'

interface CurrencyProps {
  id?: UniqueEntityID
  code: CurrencyCode
  name: CurrencyName
  isBase: boolean
}

export default class Currency extends Entity {
  private code: CurrencyCode
  private name: CurrencyName
  private isBase: boolean

  constructor({ code, id, isBase, name }: CurrencyProps) {
    super(id)
    this.code = code
    this.isBase = isBase
    this.name = name
  }

  getIsBase() {
    return this.isBase
  }

  getCode() {
    return this.code
  }
}
