import { UniqueEntityID } from 'domain/base'

export default class CartId extends UniqueEntityID {}
