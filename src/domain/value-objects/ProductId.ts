import UniqueEntityID from 'domain/base/UniqueEntityID'

export default class ProductId extends UniqueEntityID {}
