import { ValueObject } from 'domain/base'

export default class CouponMinTotal extends ValueObject<number> {
  public value: number

  private static valid(value: number) {
    return value > 0
  }

  constructor(value: number) {
    super()
    if (!CouponMinTotal.valid(value as number)) {
      throw new Error('Coupon min total invalid')
    }
    this.value = value
  }
}
