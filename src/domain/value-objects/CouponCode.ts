import { ValueObject } from 'domain/base'

export default class CouponCode extends ValueObject<string> {
  public value: string

  constructor(value: string) {
    super()
    this.value = value
  }
}
