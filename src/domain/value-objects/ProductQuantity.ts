import { Guard, ValueObject } from 'domain/base'

export default class ProductQuantity extends ValueObject<number> {
  public value: number

  private static valid(value: number) {
    return value >= 0
  }

  constructor(value: number) {
    super()
    const guard = Guard.againstNullOrUndefined(value, 'product quantity')
    if (!guard.succeeded) {
      throw new Error(guard.message)
    }
    if (!ProductQuantity.valid(value as number)) {
      throw new Error('Product quantity invalid')
    }
    this.value = value
  }
}
