import { UniqueEntityID } from 'domain/base'

export default class CartEntryId extends UniqueEntityID {}
