import { ValueObject } from 'domain/base'

export default class CurrencyCode extends ValueObject<string> {
  public value: string

  constructor(value: string) {
    super()
    this.value = value
  }
}
