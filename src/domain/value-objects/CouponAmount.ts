import { ValueObject } from 'domain/base'

export default class CouponAmount extends ValueObject<number> {
  public value: number

  private static valid(value: number) {
    return value > 0
  }

  constructor(value: number) {
    super()
    if (!CouponAmount.valid(value as number)) {
      throw new Error('Coupon amount invalid')
    }
    this.value = value
  }
}
