import { Guard, ValueObject } from 'domain/base'

export default class ProductName extends ValueObject<string> {
  public value: string

  private static isAppropriateLength(value: string) {
    return value.length > 4
  }

  constructor(value: string) {
    super()
    const guard = Guard.againstNullOrUndefined(value, 'product name')
    if (!guard.succeeded) {
      throw new Error(guard.message)
    }

    if (!ProductName.isAppropriateLength(value as string)) {
      throw new Error('Product name is too short')
    }
    this.value = value
  }
}
