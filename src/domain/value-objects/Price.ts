import { ValueObject } from 'domain/base'
import Currency from '../Currency'

interface PriceProps {
  amount: number
  currency: Currency
}
export default class Price extends ValueObject<PriceProps> {
  public value: PriceProps

  private static valid(value: number) {
    return value >= 0
  }

  constructor(value: PriceProps) {
    super()
    if (!Price.valid(value.amount)) {
      throw new Error('Price invalid')
    }
    this.value = value
  }
}
