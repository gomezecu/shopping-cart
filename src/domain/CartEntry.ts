import { Entity, UniqueEntityID } from './base'
import Product from './Product'
import { ProductId, ProductQuantity } from './value-objects'

interface CartEntryProps {
  id?: UniqueEntityID
  product: Product
  quantity: ProductQuantity
}

export default class CartEntry extends Entity {
  private readonly product: Product
  private quantity: ProductQuantity
  private static validationSameProduct = {
    quantity: 50,
    message: 'Full units by product'
  }

  private constructor({ id, product, quantity }: CartEntryProps) {
    super(id)
    this.product = product
    this.quantity = quantity
  }

  public static create(product: Product, quantity: ProductQuantity, id?: UniqueEntityID) {
    if (quantity.value > CartEntry.validationSameProduct.quantity) {
      throw new Error(CartEntry.validationSameProduct.message)
    }
    return new CartEntry({ id, product, quantity })
  }

  getProduct(): Product {
    return this.product
  }

  getQuantity() {
    return this.quantity
  }

  getTotal() {
    return this.product.getPrice().value.amount * this.quantity.value
  }

  add(quantity: ProductQuantity) {
    const quantityAux = this.quantity.value + quantity.value
    this.getProduct().hasStock(new ProductQuantity(quantityAux))
    if (quantityAux > CartEntry.validationSameProduct.quantity) {
      throw new Error(CartEntry.validationSameProduct.message)
    }
    this.quantity = new ProductQuantity(quantityAux)
  }
}
