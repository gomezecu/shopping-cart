interface CartDetailProps {
  id: string
  isComplete: boolean
  products: {
    id: string
    name: string
    price: number
    quantity: number
  }[]
  currency: string
  totalAmount?: number
  coupon?: string
}

export default class CartDetail implements CartDetailProps {
  id: string
  isComplete: boolean
  products: { id: string; name: string; price: number; quantity: number }[]
  currency: string
  totalAmount?: number = 0
  coupon?: string | undefined

  constructor({ id, isComplete, products, currency, totalAmount, coupon }: CartDetailProps) {
    this.id = id
    this.isComplete = isComplete
    this.products = products
    this.currency = currency
    this.totalAmount = totalAmount
    this.coupon = coupon
  }
}
