import Cart from 'domain/Cart'
import CartDetail from 'domain/CartDetail'
import Price from 'domain/value-objects/Price'

export default class CreateCartDetail {
  create(cart: Cart, total: Price) {
    const products = cart.getEntries().map((item) => ({
      id: item.getProduct().getId().value,
      name: item.getProduct().getName().value,
      price: item.getProduct().getPrice().value.amount,
      quantity: item.getQuantity().value
    }))

    return new CartDetail({
      id: cart.getId().value,
      isComplete: cart.getStatus(),
      products,
      totalAmount: total.value.amount,
      currency: total.value.currency.getCode().value
    })
  }
}
