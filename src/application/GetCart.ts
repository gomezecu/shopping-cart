import CartDetail from 'domain/CartDetail'
import { CartRepository, CurrencyRepository, ExchangeRateRepository } from 'domain/repositories'
import { CreateCartDetail } from 'domain/services'
import { UseCase } from './base'
import { GetCartQuery } from './queries'

export type Response = Error | Promise<CartDetail>

export default class GetCart implements UseCase<GetCartQuery, Response> {
  private repositoryCart: CartRepository
  private exchangeRateRepository: ExchangeRateRepository
  private currencyRepository: CurrencyRepository
  private createCartDetail: CreateCartDetail

  constructor(
    repositoryCart: CartRepository,
    exchangeRateRepository: ExchangeRateRepository,
    currencyRepository: CurrencyRepository,
    createCartDetail: CreateCartDetail
  ) {
    this.repositoryCart = repositoryCart
    this.exchangeRateRepository = exchangeRateRepository
    this.currencyRepository = currencyRepository
    this.createCartDetail = createCartDetail
  }

  async execute(request: GetCartQuery) {
    const cart = await this.repositoryCart.getCartById(request.cartId)
    const currency = await this.currencyRepository.findByCode(request.currencyCode)

    const hasToConvert = cart.getCurrency().getCode().value !== currency.getCode().value
    const totalCurrencyBase = hasToConvert
      ? await this.exchangeRateRepository.convert(cart.getTotal(), currency)
      : cart.getTotal()

    return this.createCartDetail.create(cart, totalCurrencyBase)
  }
}
