import { AddProductCart, RemoveCartProduct } from 'application'
import { AddProductCartCommand, RemoveCartProductCommand } from 'application/commands'
import { EventBus } from 'domain/base'
import { CartRepository, ProductRepository } from 'domain/repositories'
import { MockEventBus } from 'infrastructure/bus/tests/MockEventBus'
import { MemoryCartRepository, MemoryProductRepository } from 'infrastructure/repositories/tests'

describe('Remove Product Cart', () => {
  let repositoryCart: CartRepository
  let repositoryProduct: ProductRepository
  let eventBus: EventBus

  beforeEach(() => {
    repositoryCart = new MemoryCartRepository()
    repositoryProduct = new MemoryProductRepository()
    eventBus = new MockEventBus()
  })

  it('should remove a product', async () => {
    const useCaseAddProduct = new AddProductCart(repositoryCart, repositoryProduct, eventBus)
    const useCaseRemoveCartProduct = new RemoveCartProduct(repositoryCart, eventBus)

    const command = new AddProductCartCommand('cart1', 'id2', 2)
    await useCaseAddProduct.execute(command)
    await useCaseRemoveCartProduct.execute(new RemoveCartProductCommand('cart1', 'id2'))

    const cart = await repositoryCart.getCartById(command.cartId)
    expect(cart.getEntries()).toHaveLength(0)
  })

  it('should not remove a product which not exist', async () => {
    const useCaseAddProduct = new AddProductCart(repositoryCart, repositoryProduct, eventBus)
    const useCaseRemoveCartProduct = new RemoveCartProduct(repositoryCart, eventBus)

    const command = new AddProductCartCommand('cart1', 'id2', 2)
    await useCaseAddProduct.execute(command)
    expect(async () => {
      await useCaseRemoveCartProduct.execute(new RemoveCartProductCommand('cart1', 'id1000'))
    }).rejects.toThrow('Product not in cart')
  })

  it('should remove a product and all its related gifted products', async () => {
    const useCaseAddProduct = new AddProductCart(repositoryCart, repositoryProduct, eventBus)
    const useCaseRemoveCartProduct = new RemoveCartProduct(repositoryCart, eventBus)

    const addProductCommandA = new AddProductCartCommand('cart1', 'id1', 2)
    const addProductCommandB = new AddProductCartCommand('cart1', 'id2', 2)
    const removeProductCommandA = new RemoveCartProductCommand('cart1', 'id1')

    await useCaseAddProduct.execute(addProductCommandA)
    await useCaseAddProduct.execute(addProductCommandB)
    await useCaseRemoveCartProduct.execute(removeProductCommandA)

    const cart = await repositoryCart.getCartById(addProductCommandA.cartId)
    expect(cart.getEntries()).toHaveLength(0)
  })
})
