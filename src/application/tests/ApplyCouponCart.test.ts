import { AddProductCart, ApplyCouponCart, GetCart } from 'application'
import { AddProductCartCommand, ApplyCouponCartCommand } from 'application/commands'
import { GetCartQuery } from 'application/queries'
import { EventBus } from 'domain/base'
import Currency from 'domain/Currency'
import {
  CartRepository,
  CouponRepository,
  CurrencyRepository,
  ExchangeRateRepository,
  ProductRepository
} from 'domain/repositories'
import { CreateCartDetail } from 'domain/services'
import { Price } from 'domain/value-objects'
import { MockEventBus } from 'infrastructure/bus/tests/MockEventBus'
import {
  MemoryCartRepository,
  MemoryCouponRepository,
  MemoryCurrencyRepository,
  MemoryProductRepository
} from 'infrastructure/repositories/tests'
import { generateProducts } from 'infrastructure/repositories/tests/MemoryProduct'

class MockExchangeRateRepository implements ExchangeRateRepository {
  async convert(from: Price, to: Currency): Promise<Price> {
    return Promise.resolve(
      new Price({
        amount: from.value.amount,
        currency: to
      })
    )
  }
}

describe('Apply Coupon Cart', () => {
  let repositoryCart: CartRepository
  let repositoryCoupon: CouponRepository
  let repositoryExchangeRate: ExchangeRateRepository
  let repositoryCurrency: CurrencyRepository
  let repositoryProduct: ProductRepository
  let eventBus: EventBus
  const stockProduct = 7
  const quantityProducts = 3
  const priceProduct = 7
  const idCart = 'cart1'
  const currencyBase = 'EUR'

  beforeEach(() => {
    repositoryCart = new MemoryCartRepository()
    repositoryCoupon = new MemoryCouponRepository()
    repositoryExchangeRate = new MockExchangeRateRepository()
    repositoryCurrency = new MemoryCurrencyRepository()
    repositoryProduct = new MemoryProductRepository(generateProducts(quantityProducts, stockProduct, priceProduct))
    eventBus = new MockEventBus()
  })

  it('should not apply a coupon with when the minimum total is invalid', async () => {
    const quantityProductsToAdd = 1
    const useCaseAddProduct = new AddProductCart(repositoryCart, repositoryProduct, eventBus)
    const useCaseApplyCoupon = new ApplyCouponCart(repositoryCart, repositoryCoupon, eventBus)

    await useCaseAddProduct.execute(new AddProductCartCommand(idCart, 'id0', quantityProductsToAdd))

    expect(async () => {
      await useCaseApplyCoupon.execute(new ApplyCouponCartCommand(idCart, 'FIXED_10'))
    }).rejects.toThrow('Coupon invalid')
  })

  it('should apply a fixed discount coupon when the minimum total is valid', async () => {
    const quantityProductsToAdd = 5
    const useCaseAddProduct = new AddProductCart(repositoryCart, repositoryProduct, eventBus)
    const useCaseApplyCoupon = new ApplyCouponCart(repositoryCart, repositoryCoupon, eventBus)

    await useCaseAddProduct.execute(new AddProductCartCommand(idCart, 'id0', quantityProductsToAdd))
    await useCaseApplyCoupon.execute(new ApplyCouponCartCommand(idCart, 'FIXED_10'))

    const useCaseGetDetails = new GetCart(
      repositoryCart,
      repositoryExchangeRate,
      repositoryCurrency,
      new CreateCartDetail()
    )
    const command = new GetCartQuery(idCart, currencyBase)
    const result = await useCaseGetDetails.execute(command)
    expect(result.totalAmount).toBe(quantityProductsToAdd * priceProduct - 10)
  })

  it('should apply a percent discount coupon when the minimum total is valid', async () => {
    const quantityProductsToAdd = 5
    const useCaseAddProduct = new AddProductCart(repositoryCart, repositoryProduct, eventBus)
    const useCaseApplyCoupon = new ApplyCouponCart(repositoryCart, repositoryCoupon, eventBus)

    await useCaseAddProduct.execute(new AddProductCartCommand(idCart, 'id0', quantityProductsToAdd))
    await useCaseApplyCoupon.execute(new ApplyCouponCartCommand(idCart, 'PERCENT_50'))

    const useCaseGetDetails = new GetCart(
      repositoryCart,
      repositoryExchangeRate,
      repositoryCurrency,
      new CreateCartDetail()
    )
    const command = new GetCartQuery(idCart, currencyBase)
    const result = await useCaseGetDetails.execute(command)
    expect(result.totalAmount).toBe(quantityProductsToAdd * priceProduct * 0.5)
  })
})
