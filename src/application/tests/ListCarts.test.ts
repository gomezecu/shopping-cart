import { ListCarts } from 'application'
import { CartRepository } from 'domain/repositories'
import { CreateCartDetail } from 'domain/services'
import { MemoryCartRepository } from 'infrastructure/repositories/tests'

describe('List Carts', () => {
  let repositoryCart: CartRepository
  beforeEach(() => {
    repositoryCart = new MemoryCartRepository()
  })

  it('should list carts', async () => {
    const useCaseListCarts = new ListCarts(repositoryCart, new CreateCartDetail())

    const result = await useCaseListCarts.execute()
    const carts = await repositoryCart.findAll()
    expect(result).toHaveLength(carts.length)
  })
})
