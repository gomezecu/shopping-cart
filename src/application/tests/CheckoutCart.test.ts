import { EventBus } from 'domain/base'
import Currency from 'domain/Currency'
import { CartRepository, CurrencyRepository, ExchangeRateRepository, ProductRepository } from 'domain/repositories'
import { CreateCartDetail } from 'domain/services'
import { Price, ProductId } from 'domain/value-objects'
import { MockEventBus } from 'infrastructure/bus/tests/MockEventBus'
import {
  MemoryCartRepository,
  MemoryCurrencyRepository,
  MemoryProductRepository
} from 'infrastructure/repositories/tests'
import { generateProducts } from 'infrastructure/repositories/tests/MemoryProduct'
import AddProductCart from '../AddProductCart'
import CheckoutCart from '../CheckoutCart'
import { AddProductCartCommand, CheckoutCartCommand } from '../commands'
import GetCart from '../GetCart'
import { GetCartQuery } from '../queries'

const exchangeRateUSD = 0.8
class MockExchangeRateRepository implements ExchangeRateRepository {
  async convert(from: Price, to: Currency): Promise<Price> {
    return Promise.resolve(
      new Price({
        amount: from.value.amount * exchangeRateUSD,
        currency: to
      })
    )
  }
}

describe('Checkout Cart', () => {
  let repositoryCart: CartRepository
  let repositoryExchangeRate: ExchangeRateRepository
  let repositoryMemoryCurrency: CurrencyRepository
  let repositoryProduct: ProductRepository
  let eventBus: EventBus
  const stockProduct = 7
  const quantityProduct = 3
  const priceProduct = 7
  const idCart = 'cart1'

  beforeEach(() => {
    repositoryCart = new MemoryCartRepository()
    repositoryExchangeRate = new MockExchangeRateRepository()
    repositoryMemoryCurrency = new MemoryCurrencyRepository()
    repositoryProduct = new MemoryProductRepository(generateProducts(quantityProduct, stockProduct, priceProduct))
    eventBus = new MockEventBus()
  })

  it('should checkout the cart', async () => {
    const useCaseAddProduct = new AddProductCart(repositoryCart, repositoryProduct, eventBus)
    const useCaseCheckoutCart = new CheckoutCart(repositoryCart, repositoryProduct, eventBus)

    await useCaseAddProduct.execute(new AddProductCartCommand(idCart, 'id1', 1))
    await useCaseCheckoutCart.execute(new CheckoutCartCommand(idCart))

    const useCaseGetDetails = new GetCart(
      repositoryCart,
      repositoryExchangeRate,
      repositoryMemoryCurrency,
      new CreateCartDetail()
    )

    const result = await useCaseGetDetails.execute(new GetCartQuery(idCart, 'EUR'))
    expect(result.isComplete).toBeTruthy()

    const productId1 = await repositoryProduct.get(new ProductId('id1'))
    expect(productId1.getStock().value).toBe(stockProduct - 1)
  })
})
