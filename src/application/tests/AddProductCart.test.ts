import { AddProductCart } from 'application'
import { AddProductCartCommand } from 'application/commands'
import { EventBus } from 'domain/base'
import { CartRepository, ProductRepository } from 'domain/repositories'
import { CartId } from 'domain/value-objects'
import { MockEventBus } from 'infrastructure/bus/tests/MockEventBus'
import { MemoryCartRepository, MemoryProductRepository } from 'infrastructure/repositories/tests'
import { generateProducts } from 'infrastructure/repositories/tests/MemoryProduct'

describe('Add Product Cart', () => {
  let repositoryCart: CartRepository
  let repositoryProduct: ProductRepository
  let eventBus: EventBus

  beforeEach(() => {
    repositoryCart = new MemoryCartRepository()
    repositoryProduct = new MemoryProductRepository()
    eventBus = new MockEventBus()
  })

  it('should add a product with two units', async () => {
    const useCase = new AddProductCart(repositoryCart, repositoryProduct, eventBus)
    const command = new AddProductCartCommand('cart1', 'id2', 2)
    await useCase.execute(command)
    const cart = await repositoryCart.getCartById(command.cartId)
    expect(cart.getEntries()).toHaveLength(1)
    expect(cart.getEntries()[0].getQuantity().value).toBe(2)
  })

  it('should increment the units from a product', async () => {
    const useCase = new AddProductCart(repositoryCart, repositoryProduct, eventBus)
    const command = new AddProductCartCommand('cart1', 'id2', 2)
    await useCase.execute(command)
    await useCase.execute(command)
    const cart = await repositoryCart.getCartById(command.cartId)

    expect(cart.getEntries()).toHaveLength(1)
    expect(cart.getEntries()[0].getQuantity().value).toBe(4)
  })

  it('should add maximum ten different products', async () => {
    const command = (n: number) => new AddProductCartCommand('cart1', 'id' + n, 2)
    const useCase = new AddProductCart(repositoryCart, repositoryProduct, eventBus)
    await useCase.execute(command(0))
    await useCase.execute(command(1))
    await useCase.execute(command(2))
    await useCase.execute(command(3))
    await useCase.execute(command(4))
    await useCase.execute(command(5))
    await useCase.execute(command(6))
    await useCase.execute(command(7))
    await useCase.execute(command(8))
    await useCase.execute(command(9))
    const cart = await repositoryCart.getCartById(new CartId('cart1'))

    expect(cart.getEntries()).toHaveLength(10)
    expect(async () => {
      await useCase.execute(command(10))
    }).rejects.toThrow('Cart full of products')
  })

  it('should add maximum fifty units to the same product', async () => {
    repositoryProduct = new MemoryProductRepository(generateProducts(1, 51))
    const useCase = new AddProductCart(repositoryCart, repositoryProduct, eventBus)

    await useCase.execute(new AddProductCartCommand('cart1', 'id0', 50))

    const cart = await repositoryCart.getCartById(new CartId('cart1'))

    expect(cart.getEntries()).toHaveLength(1)
    expect(async () => {
      await useCase.execute(new AddProductCartCommand('cart1', 'id0', 1))
    }).rejects.toThrow('Full units by product')
  })

  it('should not add a product without stock in two commands', async () => {
    repositoryProduct = new MemoryProductRepository(generateProducts(1, 5))

    const useCase = new AddProductCart(repositoryCart, repositoryProduct, eventBus)

    await useCase.execute(new AddProductCartCommand('cart1', 'id0', 5))

    expect(async () => {
      await useCase.execute(new AddProductCartCommand('cart1', 'id0', 1))
    }).rejects.toThrow('Product without stock')
  })

  it('should not add a product without  stock in one command', async () => {
    repositoryProduct = new MemoryProductRepository(generateProducts(1, 5))

    const useCase = new AddProductCart(repositoryCart, repositoryProduct, eventBus)

    expect(async () => {
      await useCase.execute(new AddProductCartCommand('cart1', 'id0', 6))
    }).rejects.toThrow('Product without stock')
  })
})
