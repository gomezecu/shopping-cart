import { EventBus } from 'domain/base'
import Currency from 'domain/Currency'
import { CartRepository, CurrencyRepository, ExchangeRateRepository, ProductRepository } from 'domain/repositories'
import { CreateCartDetail } from 'domain/services'
import { Price } from 'domain/value-objects'
import { MockEventBus } from 'infrastructure/bus/tests/MockEventBus'
import {
  MemoryCartRepository,
  MemoryCurrencyRepository,
  MemoryProductRepository
} from 'infrastructure/repositories/tests'
import { generateProducts } from 'infrastructure/repositories/tests/MemoryProduct'
import AddProductCart from '../AddProductCart'
import { AddProductCartCommand } from '../commands'
import GetCart from '../GetCart'
import { GetCartQuery } from '../queries'

const exchangeRateUSD = 0.8
class MockExchangeRateRepository implements ExchangeRateRepository {
  async convert(from: Price, to: Currency): Promise<Price> {
    return Promise.resolve(
      new Price({
        amount: from.value.amount * exchangeRateUSD,
        currency: to
      })
    )
  }
}

describe('Get Cart', () => {
  let repositoryCart: CartRepository
  let repositoryExchangeRate: ExchangeRateRepository
  let repositoryMemoryCurrency: CurrencyRepository
  let repositoryProduct: ProductRepository
  let eventBus: EventBus
  const stockProduct = 7
  const quantityProduct = 3
  const priceProduct = 7
  const idCart = 'cart1'
  const currencyBase = 'EUR'

  beforeEach(() => {
    repositoryCart = new MemoryCartRepository()
    repositoryExchangeRate = new MockExchangeRateRepository()
    repositoryMemoryCurrency = new MemoryCurrencyRepository()
    repositoryProduct = new MemoryProductRepository(generateProducts(quantityProduct, stockProduct, priceProduct))
    eventBus = new MockEventBus()
  })

  it('should get cart details', async () => {
    const useCaseAddProduct = new AddProductCart(repositoryCart, repositoryProduct, eventBus)
    await useCaseAddProduct.execute(new AddProductCartCommand(idCart, 'id1', 1))

    const useCaseGetDetails = new GetCart(
      repositoryCart,
      repositoryExchangeRate,
      repositoryMemoryCurrency,
      new CreateCartDetail()
    )

    const result = await useCaseGetDetails.execute(new GetCartQuery(idCart, 'EUR'))

    expect(result).toEqual({
      id: 'cart1',
      isComplete: false,
      products: [
        {
          id: 'id1',
          name: 'product1',
          price: 7,
          quantity: 1
        }
      ],
      currency: 'EUR',
      totalAmount: 7,
      coupon: undefined
    })
  })

  it('should calculate total amount in the base currency', async () => {
    const quantityProduct1ToAdd = 3
    const quantityProduct2ToAdd = 2

    const useCaseAddProduct = new AddProductCart(repositoryCart, repositoryProduct, eventBus)
    await useCaseAddProduct.execute(new AddProductCartCommand(idCart, 'id1', quantityProduct1ToAdd))
    await useCaseAddProduct.execute(new AddProductCartCommand(idCart, 'id0', quantityProduct2ToAdd))

    const useCaseGetDetails = new GetCart(
      repositoryCart,
      repositoryExchangeRate,
      repositoryMemoryCurrency,
      new CreateCartDetail()
    )
    const result = await useCaseGetDetails.execute(new GetCartQuery(idCart, currencyBase))

    expect(result.totalAmount).toBe(quantityProduct1ToAdd * priceProduct + quantityProduct2ToAdd * priceProduct)
  })

  it('should calculate total amount in a different currency than base', async () => {
    const quantityProductToAdd = 2

    const useCaseAddProduct = new AddProductCart(repositoryCart, repositoryProduct, eventBus)
    await useCaseAddProduct.execute(new AddProductCartCommand(idCart, 'id0', quantityProductToAdd))

    const useCaseGetDetails = new GetCart(
      repositoryCart,
      repositoryExchangeRate,
      repositoryMemoryCurrency,
      new CreateCartDetail()
    )
    const result = await useCaseGetDetails.execute(new GetCartQuery(idCart, 'USD'))

    expect(result.totalAmount).toBe(quantityProductToAdd * priceProduct * exchangeRateUSD)
  })

  it('should calculate total amount skipping gifted products', async () => {
    const quantityProduct1ToAdd = 3
    const quantityProduct2ToAdd = 2

    const useCaseAddProduct = new AddProductCart(repositoryCart, repositoryProduct, eventBus)
    await useCaseAddProduct.execute(new AddProductCartCommand(idCart, 'id1', quantityProduct1ToAdd))
    await useCaseAddProduct.execute(new AddProductCartCommand(idCart, 'id2', quantityProduct2ToAdd))

    const useCaseGetDetails = new GetCart(
      repositoryCart,
      repositoryExchangeRate,
      repositoryMemoryCurrency,
      new CreateCartDetail()
    )

    const result = await useCaseGetDetails.execute(new GetCartQuery(idCart, currencyBase))

    expect(result.totalAmount).toBe(quantityProduct1ToAdd * priceProduct)
  })
})
