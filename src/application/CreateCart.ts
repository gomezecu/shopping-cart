import Cart from 'domain/Cart'
import CartDetail from 'domain/CartDetail'
import { CartRepository, CurrencyRepository } from 'domain/repositories'
import { CreateCartDetail } from 'domain/services'
import { Price } from 'domain/value-objects'
import { UseCase } from './base'

export type Response = Error | Promise<CartDetail>

export default class CreateCart implements UseCase<void, Response> {
  private repositoryCart: CartRepository
  private currencyRepository: CurrencyRepository
  private createCartDetail: CreateCartDetail

  constructor(
    repositoryCart: CartRepository,
    currencyRepository: CurrencyRepository,
    createCartDetail: CreateCartDetail
  ) {
    this.repositoryCart = repositoryCart
    this.currencyRepository = currencyRepository
    this.createCartDetail = createCartDetail
  }

  async execute() {
    const currencyBase = await this.currencyRepository.getBase()
    const cart = new Cart({
      currencyBase
    })
    this.repositoryCart.save(cart)

    return this.createCartDetail.create(
      cart,
      new Price({
        amount: 0,
        currency: currencyBase
      })
    )
  }
}
