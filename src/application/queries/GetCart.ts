import { CartId, CurrencyCode } from 'domain/value-objects'

export default class GetCart {
  private _cartId: string
  private _currency: string

  get cartId() {
    return new CartId(this._cartId)
  }

  get currencyCode() {
    return new CurrencyCode(this._currency)
  }

  constructor(cartId: string, currency: string) {
    this._cartId = cartId
    this._currency = currency
  }
}
