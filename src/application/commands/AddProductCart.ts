import { CartId, ProductId, ProductQuantity } from 'domain/value-objects'

export default class AddProductCart {
  private _cartId: string
  private _productId: string
  private _quantity: number

  get cartId() {
    return new CartId(this._cartId)
  }

  get productId() {
    return new ProductId(this._productId)
  }

  get productQuantity() {
    return new ProductQuantity(this._quantity)
  }

  constructor(cartId: string, productId: string, quantity: number) {
    this._cartId = cartId
    this._productId = productId
    this._quantity = quantity
  }
}
