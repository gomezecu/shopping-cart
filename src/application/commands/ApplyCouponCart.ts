import { CartId, CouponCode } from 'domain/value-objects'

export default class ApplyCouponCart {
  private _cartId: string
  private _code: string

  get cartId() {
    return new CartId(this._cartId)
  }

  get code() {
    return new CouponCode(this._code)
  }

  constructor(cartId: string, code: string) {
    this._cartId = cartId
    this._code = code
  }
}
