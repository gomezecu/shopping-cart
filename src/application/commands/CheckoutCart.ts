import { CartId } from 'domain/value-objects'

export default class CheckoutCart {
  private _cartId: string

  get cartId() {
    return new CartId(this._cartId)
  }

  constructor(cartId: string) {
    this._cartId = cartId
  }
}
