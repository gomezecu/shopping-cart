import { CartId, ProductId } from 'domain/value-objects'

export default class RemoveCartProduct {
  private _cartId: string
  private _productId: string

  get cartId() {
    return new CartId(this._cartId)
  }

  get productId() {
    return new ProductId(this._productId)
  }

  constructor(cartId: string, productId: string) {
    this._cartId = cartId
    this._productId = productId
  }
}
