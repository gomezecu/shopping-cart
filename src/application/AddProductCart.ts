import { EventBus } from 'domain/base'
import { CartRepository, ProductRepository } from 'domain/repositories'
import { UseCase } from './base'
import { AddProductCartCommand } from './commands'

export type Response = Error | Promise<void>

export default class AddProductCart implements UseCase<AddProductCartCommand, Response> {
  private repositoryCart: CartRepository
  private repositoryProduct: ProductRepository
  private eventBus: EventBus

  constructor(repositoryCart: CartRepository, repositoryProduct: ProductRepository, eventBus: EventBus) {
    this.repositoryCart = repositoryCart
    this.repositoryProduct = repositoryProduct
    this.eventBus = eventBus
  }

  async execute(request: AddProductCartCommand) {
    const product = await this.repositoryProduct.get(request.productId)
    const cart = await this.repositoryCart.getCartById(request.cartId)
    cart.addProduct(product, request.productQuantity)
    await this.repositoryCart.save(cart)
    this.eventBus.publish(cart.pullDomainEvents())
  }
}
