import { EventBus } from 'domain/base'
import { CartRepository } from 'domain/repositories'
import { UseCase } from './base'
import { RemoveCartProductCommand } from './commands'

export type Response = Promise<void> | Error

export default class RemoveCartProduct implements UseCase<RemoveCartProductCommand, Response> {
  private cartRepository: CartRepository
  private eventBus: EventBus

  constructor(cartRepository: CartRepository, eventBus: EventBus) {
    this.cartRepository = cartRepository
    this.eventBus = eventBus
  }

  async execute(request: RemoveCartProductCommand) {
    const cart = await this.cartRepository.getCartById(request.cartId)
    cart.removeProduct(request.productId)
    await this.cartRepository.save(cart)
    this.eventBus.publish(cart.pullDomainEvents())
  }
}
