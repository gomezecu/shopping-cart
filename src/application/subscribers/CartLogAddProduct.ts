import { DomainEventSubscriber } from 'domain/base'
import CartLogEvent from 'domain/CartLogEvent'
import { AddProductCartEvent } from 'domain/events'
import { CartLogRepository } from 'domain/repositories'

export default class CartLogAddProductSubscriber implements DomainEventSubscriber<AddProductCartEvent> {
  private repositoryCartLog: CartLogRepository

  constructor(repositoryCartLog: CartLogRepository) {
    this.repositoryCartLog = repositoryCartLog
  }

  consume(event: AddProductCartEvent): void {
    const cartLogEvent = new CartLogEvent(
      event.action,
      event.date,
      event.cartId,
      JSON.stringify({ productId: event.productId, quantity: event.quantity })
    )
    this.repositoryCartLog.saveEvent(cartLogEvent)
  }

  subscribeTo(): string {
    return AddProductCartEvent.name
  }
}
