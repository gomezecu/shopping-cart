import { DomainEventSubscriber } from 'domain/base'
import CartLogEvent from 'domain/CartLogEvent'
import { ApplyCouponCartEvent } from 'domain/events'
import { CartLogRepository } from 'domain/repositories'

export default class CartLogApplyCouponSubscriber implements DomainEventSubscriber<ApplyCouponCartEvent> {
  private repositoryCartLog: CartLogRepository

  constructor(repositoryCartLog: CartLogRepository) {
    this.repositoryCartLog = repositoryCartLog
  }

  consume(event: ApplyCouponCartEvent): void {
    const cartLogEvent = new CartLogEvent(
      event.action,
      event.date,
      event.cartId,
      JSON.stringify({ couponCode: event.couponCode })
    )
    this.repositoryCartLog.saveEvent(cartLogEvent)
  }

  subscribeTo(): string {
    return ApplyCouponCartEvent.name
  }
}
