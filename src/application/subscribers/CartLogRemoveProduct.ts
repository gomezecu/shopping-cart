import { DomainEventSubscriber } from 'domain/base'
import CartLogEvent from 'domain/CartLogEvent'
import { RemoveCartProductEvent } from 'domain/events'
import { CartLogRepository } from 'domain/repositories'

export default class CartLogRemoveProductSubscriber implements DomainEventSubscriber<RemoveCartProductEvent> {
  private repositoryCartLog: CartLogRepository

  constructor(repositoryCartLog: CartLogRepository) {
    this.repositoryCartLog = repositoryCartLog
  }

  consume(event: RemoveCartProductEvent): void {
    const cartLogEvent = new CartLogEvent(
      event.action,
      event.date,
      event.cartId,
      JSON.stringify({ productId: event.productId })
    )
    this.repositoryCartLog.saveEvent(cartLogEvent)
  }

  subscribeTo(): string {
    return RemoveCartProductEvent.name
  }
}
