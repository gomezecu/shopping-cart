import { DomainEventSubscriber } from 'domain/base'
import CartLogEvent from 'domain/CartLogEvent'
import { CheckoutCartEvent } from 'domain/events'
import { CartLogRepository } from 'domain/repositories'

export default class CartLogCheckoutSubscriber implements DomainEventSubscriber<CheckoutCartEvent> {
  private repositoryCartLog: CartLogRepository

  constructor(repositoryCartLog: CartLogRepository) {
    this.repositoryCartLog = repositoryCartLog
  }

  consume(event: CheckoutCartEvent): void {
    const cartLogEvent = new CartLogEvent(event.action, event.date, event.cartId, '')
    this.repositoryCartLog.saveEvent(cartLogEvent)
  }

  subscribeTo(): string {
    return CheckoutCartEvent.name
  }
}
