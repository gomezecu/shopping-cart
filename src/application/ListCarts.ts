import CartDetail from 'domain/CartDetail'
import { CartRepository } from 'domain/repositories'
import { CreateCartDetail } from 'domain/services'
import { UseCase } from './base'

type Response = Error | Promise<CartDetail[]>

export default class ListCarts implements UseCase<void, Response> {
  private repositoryCart: CartRepository
  private createCartDetail: CreateCartDetail

  constructor(repositoryCart: CartRepository, createCartDetail: CreateCartDetail) {
    this.repositoryCart = repositoryCart
    this.createCartDetail = createCartDetail
  }

  async execute() {
    const carts = await this.repositoryCart.findAll()

    return carts.map((item) => this.createCartDetail.create(item, item.getTotal()))
  }
}
