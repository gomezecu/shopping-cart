import { EventBus } from 'domain/base'
import { CartRepository, ProductRepository } from 'domain/repositories'
import { ProductQuantity } from 'domain/value-objects'
import { UseCase } from './base'
import { CheckoutCartCommand } from './commands'

type Response = Error | Promise<void>

export default class CheckoutCart implements UseCase<CheckoutCartCommand, Response> {
  private repositoryCart: CartRepository
  private repositoryProduct: ProductRepository
  private eventBus: EventBus

  constructor(repositoryCart: CartRepository, repositoryProduct: ProductRepository, eventBus: EventBus) {
    this.repositoryCart = repositoryCart
    this.repositoryProduct = repositoryProduct
    this.eventBus = eventBus
  }

  async execute(request: CheckoutCartCommand) {
    const cart = await this.repositoryCart.getCartById(request.cartId)
    cart.checkout()
    const products = cart.getEntries().map((entry) => {
      const newStock = entry.getProduct().getStock().value - entry.getQuantity().value
      if (newStock < 0) {
        throw new Error('Product without stock')
      }
      const product = entry.getProduct()
      product.setStock(new ProductQuantity(newStock))
      return product
    })
    await this.repositoryProduct.saveCollection(products)
    await this.repositoryCart.save(cart)
    this.eventBus.publish(cart.pullDomainEvents())
  }
}
