import { EventBus } from 'domain/base'
import { CartRepository, CouponRepository } from 'domain/repositories'
import { UseCase } from './base'
import { ApplyCouponCartCommand } from './commands'

type Response = Error | Promise<void>

export default class ApplyCouponCart implements UseCase<ApplyCouponCartCommand, Response> {
  private repositoryCart: CartRepository
  private repositoryCoupon: CouponRepository
  private eventBus: EventBus

  constructor(repositoryCart: CartRepository, repositoryCoupon: CouponRepository, eventBus: EventBus) {
    this.repositoryCart = repositoryCart
    this.repositoryCoupon = repositoryCoupon
    this.eventBus = eventBus
  }

  async execute(request: ApplyCouponCartCommand) {
    const cart = await this.repositoryCart.getCartById(request.cartId)
    const coupon = await this.repositoryCoupon.findByCode(request.code)
    cart.applyCoupon(coupon)
    await this.repositoryCart.save(cart)
    this.eventBus.publish(cart.pullDomainEvents())
  }
}
